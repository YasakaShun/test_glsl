#include "MainView.hpp"

#include "GL/glew.h"
#include "GL/glut.h"

#include "glutils.hpp"
#include "scenebasic.hpp"
#include "scenebasic_layout.hpp"
#include "scenebasic_uniform.hpp"
#include "scenebasic_uniformblock.hpp"

#include <iostream>
using std::cout;
using std::endl;
#include <cstdio>

namespace
{
    Scene* scene;

    void display()
    {
        GLUtils::checkForOpenGLError(__FILE__, __LINE__);
        scene->render();
        glFlush();
    }
}


MainView::MainView(int aArgc, char** aArgv)
{
    initializeGL(aArgc, aArgv);
}

void MainView::initializeGL(int aArgc, char** aArgv)
{
    glutInit(&aArgc, aArgv);
    glutInitWindowPosition(100, 50);
    glutInitWindowSize(600, 600);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH);

    glutCreateWindow("Chapter 1 Examples");
    glutDisplayFunc(display);

    {
        //////////////// PLUG IN SCENE HERE /////////////////
        //scene = new SceneBasic();
        //scene = new SceneBasic_Layout();
        //scene = new SceneBasic_Uniform();
        scene = new SceneBasic_UniformBlock();
        ////////////////////////////////////////////////////

        GLenum err = glewInit();
        if (GLEW_OK != err) {
            cout << "Error initializing GLEW: " << glewGetErrorString(err) << endl;
            GLUtils::checkForOpenGLError(__FILE__, __LINE__);
        }
        GLUtils::checkForOpenGLError(__FILE__, __LINE__);

        GLUtils::dumpGLInfo();

        glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

        scene->initScene();
    }

    glutMainLoop();

}
