#pragma once

#include "scene.hpp"

class MainView
{
public:
    MainView(int aArgc, char** aArgv);

private:
    void initializeGL(int aArgc, char** aArgv);
};