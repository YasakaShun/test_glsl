#pragma once

#include <gl/glew.h>
#include <GL/glut.h>

#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

class GLSL
{
public:
    static const GLsizei Width = 600;
    static const GLsizei Height = 600;

    typedef GLfloat Position[5];
    typedef GLuint Edge[2];
    typedef GLuint Face[3];

    static GLSL* Instance()
    {
        return instance;
    }

    static void Create()
    {
        if (!instance) {
            instance = new GLSL();
        }
    }

    static void Destroy()
    {
        delete instance;
        instance = nullptr;
    }

    void run(int aArgc, char** aArgv);

    /// ���擾
    //@{
    GLuint programId() const { return mProgramID; }
    GLuint fragmentShaderId() const { return mFragmentShaderID; }
    GLuint vertexShaderId() const { return mVertexShaderID; }

    const GLfloat* projectionMatrix() const { return mProjectionMatrix; }
    const GLfloat* lightDirection() const { return LightDirection; }
    const GLfloat* lightColor() const { return LightColor; }

    const GLuint vertexBufferPosition() const { return mVertexBuffer[0]; }
    const GLuint vertexBufferEdge() const { return mVertexBuffer[1]; }
    Position* position() const { return mPosition; }

    GLuint pointNum() const { return mPointNum; }

    GLint projectionMatrixLocation() const { return mProjectionMatrixLocation; }
    GLint lightDirectionLocation() const { return mLightDirectionLocation; }
    GLint lightColorLocation() const { return mLightColorLocation; }
    //@}

private:
    static GLSL* instance;
    static const GLfloat LightDirection[];
    static const GLfloat LightColor[];

    GLSL();

    void init();
    void prepare_vertex_shader();
    void prepare_fragment_shader();
    void link_program();

    GLuint initWireCube();
    GLuint initWireSphere(int aSlices, int aStacks);
    GLuint initSolidSphere(int aSlices, int aStacks);

    GLuint mProgramID;
    GLuint mVertexShaderID;
    GLuint mFragmentShaderID;

    GLfloat mProjectionMatrix[16];

    GLuint mVertexBuffer[2];
    Position* mPosition;
    Edge *mEdge;
    Face *mFace;
    GLuint mPointNum;

    GLint mProjectionMatrixLocation;
    GLint mLightDirectionLocation;
    GLint mLightColorLocation;
};