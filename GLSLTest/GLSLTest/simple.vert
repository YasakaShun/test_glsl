#version 460 core

invariant gl_Position;

uniform mat4 projectionMatrix;
uniform vec3 lightDirection;
uniform vec3 lightColor;
uniform vec3 diffuseMaterial = vec3(0.0, 1.0, 1.0);

in vec3 position;
in vec2 texture;

out vec3 diffuseColor;
out vec2 t;

void main()
{
    t = texture;
    diffuseColor = vec3(dot(position, lightDirection)) * lightColor;
    gl_Position = projectionMatrix * vec4(position, 1.0);
}
