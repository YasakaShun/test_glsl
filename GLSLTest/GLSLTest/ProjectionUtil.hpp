#pragma once

#include <GL/glut.h>

class ProjectionUtil
{
public:
    // ���s���e�ϊ��s������߂�
    static void OrthogonalMatrix(
        float aLeft,
        float aRight,
        float aBottom,
        float aTop,
        float aNear,
        float aFar,
        GLfloat* aMatrix
    );

    // �������e�ϊ��s������߂�
    static void PerspectiveMatrix(
        float aLeft,
        float aRight,
        float aBottom,
        float aTop,
        float aNear,
        float aFar,
        GLfloat* aMatrix
    );

    // ����ϊ��s������߂�
    static void LookAt(float aEx, float aEy, float aEz,
        float aTx, float aTy, float aTz,
        float aUx, float aUy, float aUz,
        GLfloat *aMatrix
    );

    static void MultiplyMatrix(
        const GLfloat* aM0, const GLfloat* aM1, GLfloat* aMatrix
    );
};