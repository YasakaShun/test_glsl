#include "ProjectionUtil.hpp"

#include <math.h>

//----------------------------------------------------------------
void ProjectionUtil::OrthogonalMatrix(
    float aLeft,
    float aRight,
    float aBottom,
    float aTop,
    float aNear,
    float aFar,
    GLfloat* aMatrix
)
{
    float dx = aRight - aLeft;
    float dy = aTop - aBottom;
    float dz = aFar - aNear;

    for (int i = 0; i < 16; ++i) {
        aMatrix[i] = 0.0f;
    }

    aMatrix[0] = 2.0f / dx;
    aMatrix[5] = 2.0f / dy;
    aMatrix[10] = -2.0f / dz;
    aMatrix[12] = -(aRight + aLeft) / dx;
    aMatrix[13] = -(aTop + aBottom) / dy;
    aMatrix[14] = -(aFar + aNear) / dz;
    aMatrix[15] = 1.0f;
}

//----------------------------------------------------------------
void ProjectionUtil::PerspectiveMatrix(
    float aLeft,
    float aRight,
    float aBottom,
    float aTop,
    float aNear,
    float aFar,
    GLfloat *aMatrix
)
{
    float dx = aRight - aLeft;
    float dy = aTop - aBottom;
    float dz = aFar - aNear;

    for (int i = 0; i < 16; ++i) {
        aMatrix[i] = 0.0f;
    }
    aMatrix[0] = 2.0f * aNear / dx;
    aMatrix[5] = 2.0f * aNear / dy;
    aMatrix[8] = (aRight + aLeft) / dx;
    aMatrix[9] = (aTop + aBottom) / dy;
    aMatrix[10] = -(aFar + aNear) / dz;
    aMatrix[11] = -1.0f;
    aMatrix[14] = -2.0f * aFar * aNear / dz;
}

//----------------------------------------------------------------
void ProjectionUtil::LookAt(
    float ex, float ey, float ez,
    float tx, float ty, float tz,
    float ux, float uy, float uz,
    GLfloat *matrix
)
{
    /* z �� = e - t */
    tx = ex - tx;
    ty = ey - ty;
    tz = ez - tz;
    float l = sqrtf(tx * tx + ty * ty + tz * tz); /* ���� l ��, */
    matrix[2] = tx / l;
    matrix[6] = ty / l;
    matrix[10] = tz / l;

    /* x �� = u x z �� */
    tx = uy * matrix[10] - uz * matrix[6];
    ty = uz * matrix[2] - ux * matrix[10];
    tz = ux * matrix[6] - uy * matrix[2];
    l = sqrtf(tx * tx + ty * ty + tz * tz); /* ���� l. */
    matrix[0] = tx / l;
    matrix[4] = ty / l;
    matrix[8] = tz / l;

    /* y �� = z �� x x �� */
    matrix[1] = matrix[6] * matrix[8] - matrix[10] * matrix[4];
    matrix[5] = matrix[10] * matrix[0] - matrix[2] * matrix[8];
    matrix[9] = matrix[2] * matrix[4] - matrix[6] * matrix[0];

    /* ���s�ړ� */
    matrix[12] = -(ex * matrix[0] + ey * matrix[4] + ez * matrix[8]);
    matrix[13] = -(ex * matrix[1] + ey * matrix[5] + ez * matrix[9]);
    matrix[14] = -(ex * matrix[2] + ey * matrix[6] + ez * matrix[10]);

    /* �c�� */
    matrix[3] = matrix[7] = matrix[11] = 0.0f;
    matrix[15] = 1.0f;
}

//----------------------------------------------------------------
void ProjectionUtil::MultiplyMatrix(
    const GLfloat* m0, const GLfloat* m1, GLfloat* matrix
)
{
    for (int i = 0; i < 16; ++i) {
        int j = i & ~3, k = i & 3;

        matrix[i] = m0[j + 0] * m1[0 + k]
            + m0[j + 1] * m1[4 + k]
            + m0[j + 2] * m1[8 + k]
            + m0[j + 3] * m1[12 + k];
    }
}