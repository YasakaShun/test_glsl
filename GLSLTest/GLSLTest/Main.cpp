#include <windows.h>

#include "glsl.hpp"

int main(int argc, char ** argv)
{
    GLSL::Create();

    GLSL* glsl = GLSL::Instance();
    glsl->run(argc, argv);

    GLSL::Destroy();

    return 0;
}

