#version 460 core

in vec3 diffuseColor;
in vec2 t;

out vec4 FragColor;

const vec3 c1 = vec3(1.0, 0.0, 0.0);
const vec3 c2 = vec3(0.0, 0.0, 1.0);

void main()
{
    if (mod(floor(t.x * 12.0) + floor(t.y * 12.0), 2.0) == 0.0) {
        discard;
    }
    FragColor = vec4(diffuseColor * c1, 1.0f);
}
