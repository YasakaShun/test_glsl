#include "glsl.hpp"

#include "ProjectionUtil.hpp"

#include <stdio.h>
#include <stdlib.h>

// VisualStudio で fopen を使うために警告を抑制
#pragma warning(disable: 4996)

namespace
{

/// 描画処理
void display()
{
    GLSL* glsl = GLSL::Instance();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glViewport(0, 0, GLSL::Width, GLSL::Height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // シェーダを使う
    glUseProgram(glsl->programId());

    // uniform 変数に値を設定する
    glUniformMatrix4fv(glsl->projectionMatrixLocation(), 1, GL_FALSE, glsl->projectionMatrix());
    glUniform3fv(glsl->lightDirectionLocation(), 1, glsl->lightDirection());
    glUniform3fv(glsl->lightColorLocation(), 1, glsl->lightColor());

    // index が 0 と 1 の attribute 変数に頂点情報を対応付ける
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    // 頂点バッファオブジェクトの座標として glsl->vertexBufferPosition() を指定する
    glBindBuffer(GL_ARRAY_BUFFER, glsl->vertexBufferPosition());

    // 頂点情報の格納場所と書式を指定する
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, (GLfloat*)nullptr + 3);

    // 頂点バッファオブジェクトの指標として glsl->vertexBufferEdge() を指定する
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glsl->vertexBufferEdge());

    // 図形の描画
    // glDrawElements(GL_LINES, glsl->pointNum(), GL_UNSIGNED_INT, 0);
    glDrawElements(GL_TRIANGLES, glsl->pointNum(), GL_UNSIGNED_INT, 0);

    // 頂点バッファオブジェクトを解放する
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // index が 0 と 1 の attribute 変数の頂点情報との対応付けを解除する
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    glDisable(GL_DEPTH_TEST);

    glFlush();
}

/// 常時処理
void idle()
{
}

}

GLSL* GLSL::instance = nullptr;
const GLfloat GLSL::LightDirection[] = { 0.83f, 0.50f, 0.25f };
const GLfloat GLSL::LightColor[] = { 1.0f, 1.0f, 1.0f };

GLSL::GLSL()
: mProgramID(0)
, mVertexShaderID(0)
, mFragmentShaderID(0)
, mProjectionMatrix()
, mVertexBuffer()
, mPosition()
, mEdge()
, mFace()
, mPointNum(0)
, mProjectionMatrixLocation(0)
, mLightDirectionLocation(0)
, mLightColorLocation(0)
{
}

void GLSL::run(int aArgc, char** aArgv)
{
    glutInit(&aArgc, aArgv);
    glutInitWindowPosition(100, 50);
    glutInitWindowSize(GLSL::Width, GLSL::Height);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH);

    glutCreateWindow("Hello GLUT!!");
    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glewInit();
    init();
    glutMainLoop();
}

void GLSL::init()
{
    //頂点シェーダの準備
    prepare_vertex_shader();
    //フラグメントシェーダの準備
    prepare_fragment_shader();
    //プログラムのリンク
    link_program();

    glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
}

void GLSL::prepare_vertex_shader()
{

    //////////////////////////////////////////////
    // シェーダを作ります
    // (作るといっても宣言みたいなもの)
    mVertexShaderID = glCreateShader(GL_VERTEX_SHADER);

    ////////////////////////////////////////////
    // ファイルから頂点シェーダを読み込みます。
    // 注意　ここはSTLのifstreamとかstringstreamの使い方の話で、
    // OpenGL命令は一つも無い。
    const char * vertex_file_path = "Q:\\test_glsl\\GLSLTest\\GLSLTest\\simple.vert";
    std::string VertexShaderCode;
    std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
    if (VertexShaderStream.is_open()) {
        std::stringstream sstr;
        sstr << VertexShaderStream.rdbuf();
        VertexShaderCode = sstr.str();
        VertexShaderStream.close();
    }
    ////////////////////////////////////////////
    // 頂点シェーダをコンパイルします。
    printf("Compiling shader : %s\n", vertex_file_path);
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(mVertexShaderID, 1, &VertexSourcePointer, nullptr);
    glCompileShader(mVertexShaderID);

    ////////////////////////////////////////////
    // エラーチェック
    GLint Result = GL_FALSE;
    int InfoLogLength;

    // 頂点シェーダをチェックします。
    glGetShaderiv(mVertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(mVertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if (Result == FALSE) {
        std::vector<char> VertexShaderErrorMessage(InfoLogLength);
        glGetShaderInfoLog(mVertexShaderID, InfoLogLength, nullptr, &VertexShaderErrorMessage[0]);
        fprintf(stdout, "%s\n", &VertexShaderErrorMessage[0]);
    }

}

void GLSL::prepare_fragment_shader()
{

    /////////////////////////////////////////////
    // シェーダを作ります。
    mFragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    /////////////////////////////////////////////
    // ファイルからフラグメントシェーダを読み込みます。
    const char * fragment_file_path = "Q:\\test_glsl\\GLSLTest\\GLSLTest\\simple.frag";
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
    if (FragmentShaderStream.is_open()) {
        std::stringstream sstr;
        sstr << FragmentShaderStream.rdbuf();
        FragmentShaderCode = sstr.str();
        FragmentShaderStream.close();
    }

    /////////////////////////////////////////////
    // フラグメントシェーダをコンパイルします。
    printf("Compiling shader : %s\n", fragment_file_path);
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(mFragmentShaderID, 1, &FragmentSourcePointer, NULL);
    glCompileShader(mFragmentShaderID);

    GLint Result = GL_FALSE;
    int InfoLogLength;

    /////////////////////////////////////////////
    // フラグメントシェーダをチェックします。
    glGetShaderiv(mFragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(mFragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if (Result == GL_FALSE) {
        std::vector<char> FragmentShaderErrorMessage(InfoLogLength);
        glGetShaderInfoLog(mFragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
        fprintf(stdout, "%s\n", &FragmentShaderErrorMessage[0]);
    }
}

void GLSL::link_program()
{

    GLint Result = GL_FALSE;
    int InfoLogLength;

    ////////////////////////////////////////
    // プログラムをリンクします。
    fprintf(stdout, "Linking program\n");
    mProgramID = glCreateProgram();
    glAttachShader(mProgramID, mVertexShaderID);
    glAttachShader(mProgramID, mFragmentShaderID);

    // attribute 変数 position の index を 0 に指定する
    glBindAttribLocation(mProgramID, 0, "position");
    // attribute 変数 texture の index を 1 に指定する
    glBindAttribLocation(mProgramID, 1, "texture");

    glLinkProgram(mProgramID);
    //GLint positionIndex = glGetAttribLocation(mProgramID, "position");

    ////////////////////////////////////////
    // プログラムをチェックします。
    glGetProgramiv(mProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(mProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> ProgramErrorMessage((std::max)(InfoLogLength, int(1)));
    glGetProgramInfoLog(mProgramID, InfoLogLength, nullptr, &ProgramErrorMessage[0]);
    fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);

    GLfloat viewMatrix[16];
    GLfloat projMatrix[16];

    // 視野変換行列を求める
    ProjectionUtil::LookAt(
        6.0f, 5.0f, 4.0f,
        0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        viewMatrix
    );
    // 投影変換行列を求める
    // ProjectionUtil::OrthogonalMatrix(-1.0f, 1.0f, -1.0f, 1.0f, 7.0f, 11.0f, projMatrix);
    ProjectionUtil::PerspectiveMatrix(-2.0f, 2.0f, -2.0f, 2.0f, 7.0f, 11.0f, projMatrix);
    // mProjectionMatrix = viewMatrix * projMatrix
    ProjectionUtil::MultiplyMatrix(viewMatrix, projMatrix, mProjectionMatrix);

    // uniform 変数 projectionMatrix の場所を得る
    mProjectionMatrixLocation = glGetUniformLocation(mProgramID, "projectionMatrix");
    mLightDirectionLocation = glGetUniformLocation(mProgramID, "lightDirection");
    mLightColorLocation = glGetUniformLocation(mProgramID, "lightColor");

    glGenBuffers(2, mVertexBuffer);

    // mPointNum = initWireCube();
    // mPointNum = initWireSphere(12, 12);
    mPointNum = initSolidSphere(12, 12);
}

GLuint GLSL::initWireCube()
{
    // 頂点バッファオブジェクトに8頂点分のメモリ領域を確保する
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mVertexBuffer[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Position) * 8, nullptr, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Edge) * 12, nullptr, GL_STATIC_DRAW);
    // 頂点バッファオブジェクトのメモリをプログラムのメモリ空間にマップする
    mPosition = (Position*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    mEdge = (Edge*)glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

    // 頂点バッファオブジェクトのメモリにデータを書き込む
    mPosition[0][0] = -1.0f;
    mPosition[0][1] = -1.0f;
    mPosition[0][2] = -1.0f;

    mPosition[1][0] = 1.0f;
    mPosition[1][1] = -1.0f;
    mPosition[1][2] = -1.0f;

    mPosition[2][0] = 1.0f;
    mPosition[2][1] = -1.0f;
    mPosition[2][2] = 1.0f;

    mPosition[3][0] = -1.0f;
    mPosition[3][1] = -1.0f;
    mPosition[3][2] = 1.0f;

    mPosition[4][0] = -1.0f;
    mPosition[4][1] = 1.0f;
    mPosition[4][2] = -1.0f;

    mPosition[5][0] = 1.0f;
    mPosition[5][1] = 1.0f;
    mPosition[5][2] = -1.0f;

    mPosition[6][0] = 1.0f;
    mPosition[6][1] = 1.0f;
    mPosition[6][2] = 1.0f;

    mPosition[7][0] = -1.0f;
    mPosition[7][1] = 1.0f;
    mPosition[7][2] = 1.0f;

    // 頂点バッファオブジェクトのメモリにデータを書き込む
    mEdge[0][0] = 0;
    mEdge[0][1] = 1;

    mEdge[1][0] = 1;
    mEdge[1][1] = 2;

    mEdge[2][0] = 2;
    mEdge[2][1] = 3;

    mEdge[3][0] = 3;
    mEdge[3][1] = 0;

    mEdge[4][0] = 0;
    mEdge[4][1] = 4;

    mEdge[5][0] = 1;
    mEdge[5][1] = 5;

    mEdge[6][0] = 2;
    mEdge[6][1] = 6;

    mEdge[7][0] = 3;
    mEdge[7][1] = 7;

    mEdge[8][0] = 4;
    mEdge[8][1] = 5;

    mEdge[9][0] = 5;
    mEdge[9][1] = 6;

    mEdge[10][0] = 6;
    mEdge[10][1] = 7;

    mEdge[11][0] = 7;
    mEdge[11][1] = 4;

    // 頂点バッファオブジェクトのメモリをプログラムのメモリ空間から切り離し、解放する。
    // 指標データを先に切り離し・解放してから座標データを切り離し・解放する。
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glUnmapBuffer(GL_ARRAY_BUFFER);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return 24;
}

GLuint GLSL::initWireSphere(const int aSlices, const int aStacks)
{
    GLuint vertices = aSlices * (aStacks - 1) + 2;
    GLuint edges = aSlices * (aStacks - 1) * 2 + aSlices;

    // 頂点バッファオブジェクトに8頂点分のメモリ領域を確保する
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mVertexBuffer[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Position) * vertices, nullptr, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Edge) * edges, nullptr, GL_STATIC_DRAW);

    // 頂点バッファオブジェクトのメモリをプログラムのメモリ空間にマップする
    mPosition = (Position*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    mEdge = (Edge*)glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

    // 頂点バッファオブジェクトのメモリにデータを書き込む
    mPosition[0][0] = 0.0f;
    mPosition[0][1] = 1.0f;
    mPosition[0][2] = 0.0f;

    mPosition[vertices - 1][0] = 0.0f;
    mPosition[vertices - 1][1] = -1.0f;
    mPosition[vertices - 1][2] = 0.0f;

    for (int i = 0; i < aStacks - 1; ++i) {
        for (int j = 0; j < aSlices; ++j) {
            const int vNum = aSlices * i + j + 1;
            const float pi = 3.141592653f;
            float ph = 180.0f * float(i + 1) / float(aStacks);
            float th = 360.0f * float(j + 1) / float(aSlices);
            ph = ph * pi / 180.0f;
            th = th * pi / 180.0f;

            const float r = sinf(ph);
            mPosition[vNum][0] = r * cosf(th);
            mPosition[vNum][1] = cosf(ph);
            mPosition[vNum][2] = r * sinf(th);
        }
    }

    // 頂点バッファオブジェクトのメモリにデータを書き込む
    for (int j = 0; j < aSlices; ++j) {
        mEdge[j][0] = 0;
        mEdge[j][1] = j + 1;
    }
    for (int i = 0; i < aStacks - 1; ++i) {
        for (int j = 0; j <= aSlices; ++j) {
            const int vNum = aSlices * i + j + 1;
            const int eNum = aSlices + (aSlices * i + j) * 2;
            // 横方向
            if (j == aSlices - 1) {
                mEdge[eNum][0] = vNum;
                mEdge[eNum][1] = vNum + 1 - aSlices;
            } else {
                mEdge[eNum][0] = vNum;
                mEdge[eNum][1] = vNum + 1;
            }
            // 縦方向
            if (i == aStacks - 2) {
                mEdge[eNum + 1][0] = vNum;
                mEdge[eNum + 1][1] = vertices - 1;
            } else {
                mEdge[eNum + 1][0] = vNum;
                mEdge[eNum + 1][1] = vNum + aSlices;
            }
        }
    }

    printf("vertex\n");
    for (GLuint i = 0; i < vertices; ++i) {
        printf("(%f, %f, %f)\n", mPosition[i][0], mPosition[i][1], mPosition[i][2]);
    }
    printf("edge\n");
    for (GLuint i = 0; i < edges; ++i) {
        printf("(%d -> %d)\n", mEdge[i][0], mEdge[i][1]);
    }

    // 頂点バッファオブジェクトのメモリをプログラムのメモリ空間から切り離し、解放する。
    // 指標データを先に切り離し・解放してから座標データを切り離し・解放する。
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glUnmapBuffer(GL_ARRAY_BUFFER);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return edges * 2;
}

GLuint GLSL::initSolidSphere(const int aSlices, const int aStacks)
{
    GLuint vertices = (aSlices + 1) * (aStacks + 1);
    GLuint faces = aSlices * aStacks * 2;

    // 頂点バッファオブジェクトに8頂点分のメモリ領域を確保する
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mVertexBuffer[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Position) * vertices, nullptr, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Face) * faces, nullptr, GL_STATIC_DRAW);

    // 頂点バッファオブジェクトのメモリをプログラムのメモリ空間にマップする
    mPosition = (Position*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    mFace = (Face*)glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

    // 頂点
    for (int i = 0; i <= aStacks; ++i) {
        for (int j = 0; j <= aSlices; ++j) {
            const int vNum = (aSlices + 1) * i + j;
            const float pi = 3.141592653f;
            float ph = 180.0f * float(i) / float(aStacks);
            float th = 360.0f * float(j) / float(aSlices);
            ph = ph * pi / 180.0f;
            th = th * pi / 180.0f;

            const float r = sinf(ph);
            mPosition[vNum][0] = r * cosf(th);
            mPosition[vNum][1] = cosf(ph);
            mPosition[vNum][2] = r * sinf(th);
            mPosition[vNum][3] = float(i) / (float)aStacks;
            mPosition[vNum][4] = float(j) / (float)aSlices;
        }
    }

    // フェース
    for (int i = 0; i < aStacks; ++i) {
        for (int j = 0; j < aSlices; ++j) {
            const int vNum = (aSlices + 1) * i + j;
            const int fNum = (aSlices * i + j) * 2;
            // 上側
            mFace[fNum][0] = vNum;
            mFace[fNum][1] = vNum + aSlices + 2;
            mFace[fNum][2] = vNum + 1;

            // 下側
            mFace[fNum + 1][0] = vNum;
            mFace[fNum + 1][1] = vNum + aSlices + 1;
            mFace[fNum + 1][2] = vNum + aSlices + 2;
        }
    }

    printf("vertex\n");
    for (GLuint i = 0; i < vertices; ++i) {
        printf("(%f, %f, %f, %f, %f)\n", mPosition[i][0], mPosition[i][1], mPosition[i][2], mPosition[i][3], mPosition[i][4]);
    }
    printf("face\n");
    for (GLuint i = 0; i < faces; ++i) {
        printf("(%d, %d, %d)\n", mFace[i][0], mFace[i][1], mFace[i][2]);
    }

    // 頂点バッファオブジェクトのメモリをプログラムのメモリ空間から切り離し、解放する。
    // 指標データを先に切り離し・解放してから座標データを切り離し・解放する。
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glUnmapBuffer(GL_ARRAY_BUFFER);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return faces * 3;
}

