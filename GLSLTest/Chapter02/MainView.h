#pragma once

#include "scene.h"

class MainView
{
public:
    MainView(int aArgc, char** aArgv);

private:
    void initializeGL(int aArgc, char** aArgv);
};