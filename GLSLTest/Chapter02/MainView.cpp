#include "MainView.h"

#include "GL/glew.h"
#include "GL/glut.h"

#include "glutils.h"
#include "scenediscard.h"
#include "scenediffuse.h"
#include "sceneads.h"
#include "sceneflat.h"
#include "scenesubroutine.h"
#include "scenetwoside.h"

#include <iostream>
using std::cout;
using std::endl;
#include <cstdio>

namespace
{
    Scene* scene;

    void display()
    {
        GLUtils::checkForOpenGLError(__FILE__, __LINE__);
        scene->render();
        glFlush();
    }

    void resize(int w, int h)
    {
        GLUtils::checkForOpenGLError(__FILE__, __LINE__);
        scene->resize(w, h);
        glFlush();
    }
}


MainView::MainView(int aArgc, char** aArgv)
{
    initializeGL(aArgc, aArgv);
}

void MainView::initializeGL(int aArgc, char** aArgv)
{
    glutInit(&aArgc, aArgv);
    glutInitWindowPosition(100, 50);
    glutInitWindowSize(600, 600);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH);

    glutCreateWindow("Chapter 2 Examples");
    glutDisplayFunc(display);
    glutReshapeFunc(resize);

    {
        //////////////// PLUG IN SCENE HERE /////////////////
        // scene = new SceneDiffuse();
        // scene = new SceneADS();
        // scene = new SceneTwoSide();
        // scene = new SceneFlat();
        // scene = new SceneDiscard();
        scene = new SceneSubroutine();
        ////////////////////////////////////////////////////

        GLenum err = glewInit();
        if (GLEW_OK != err) {
            cout << "Error initializing GLEW: " << glewGetErrorString(err) << endl;
            GLUtils::checkForOpenGLError(__FILE__, __LINE__);
        }
        GLUtils::checkForOpenGLError(__FILE__, __LINE__);

        GLUtils::dumpGLInfo();

        glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

        scene->initScene();
    }

    glutMainLoop();

}
